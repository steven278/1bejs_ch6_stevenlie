const isRoleAllowed = (roles) => {
    return (req, res, next) => {
        let allowed = false;
        roles.forEach(role => {
            if (role === req.user.role_id) {
                allowed = true;
            }
        })
        if (!allowed) {
            res.status(403).json({
                status: 'Forbidden',
                message: 'You don\'t have permission to access this resource.'
            })
        }
        next();
    }
}

module.exports = isRoleAllowed;