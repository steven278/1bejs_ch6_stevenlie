require('dotenv').config();
const passport = require('passport');
const { User, Profile, History, Authentication } = require('../models');
const bcrypt = require('bcrypt');
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_SECRET;
opts.passReqToCallback = true;

const isUpdateDeleteAuthorized = async (req, path, paramsId, payload) => {
    if (path === 'histories') {
        const history = await History.findOne({ where: { id: req.params.id } });
        // return req.method !== 'POST' && history.user_id != payloadId && payloadRoleId != 1;
        return history.user_id != payload.id && payload.role_id != 1;
    }
    // return req.method !== 'POST' && paramsId != payloadId && payloadRoleId != 1;
    return paramsId != payload.id && payload.role_id != 1;
}

const isgetByIdAuthorized = async (path, paramsId, payload) => {
    if (paramsId) {
        if (path === 'users') {
            const user = await User.findOne({ where: { id: paramsId } });
            return payload.id != user.id && payload.role_id != 1;
        } else if (path === 'profiles') {
            const profile = await Profile.findOne({ where: { id: paramsId } });
            return profile.user_id != payload.id && payload.role_id != 1;
        } else {
            const history = await History.findOne({ where: { id: paramsId } });
            return history.user_id != payload.id && payload.role_id != 1;
        }
    }
    return false;
}

passport.use(new JwtStrategy(opts, async (req, jwt_payload, done) => {
    try {
        const path = req.baseUrl.split('/')[3];
        if (Date.now() > jwt_payload.exp) throw new Error('Your token has expired');
        const user = await User.findOne({ where: { email: jwt_payload.email } });
        if (!user) throw new Error('user not found');
        if (path === 'change-password') {
            const auth = await Authentication.findOne({ where: { user_id: user.id } });
            const match = await bcrypt.compare(auth.otp, jwt_payload.otp);
            if (!match) {
                throw new Error('You are not authorized to access this resource');
            }
        } else {
            const unauthorizedGetById = req.method === 'GET' ? await isgetByIdAuthorized(path, req.params.id, jwt_payload) : false;
            const unauthorizedCreate = req.method === 'POST' ? req.body.user_id != jwt_payload.id && jwt_payload.role_id != 1 : false;
            const unauthorizedUpdateDelete = req.method === 'PUT' || req.method === 'DELETE' ? await isUpdateDeleteAuthorized(req, path, req.params.id, jwt_payload) : false;

            if (unauthorizedGetById || unauthorizedCreate || unauthorizedUpdateDelete) {
                throw new Error('You are not authorized to access this resource');
            }
        }
        return done(null, user);
    } catch (err) {
        return done(err, false);
    }
}));

module.exports = passport.authenticate('jwt', { session: false });