require('dotenv').config();
const { User } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const login = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        const foundUser = await User.findOne({ where: { email } });
        const isValidPassword = await bcrypt.compare(password, foundUser.password)
        if (isValidPassword) {
            const payload = {
                id: foundUser.id,
                role_id: foundUser.role_id,
                username: foundUser.username,
                email: foundUser.email,
                iat: Date.now()
            }
            const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 3000000 });
            return res.status(200).json({ token })
        }
        throw new Error();
    } catch (err) {
        return res.status(400).json({
            status: 'Failed',
            message: 'Wrong email or password'
        });
    }
}

module.exports = login;