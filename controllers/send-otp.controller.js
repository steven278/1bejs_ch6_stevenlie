const { User, Authentication } = require('../models');
const mailer = require('../helper/mailer')

const generateOTP = () => {
    const arr = [];
    for (let i = 0; i <= 5; i++) {
        arr.push(Math.round(Math.random() * 9).toString());
    }
    return arr.toString().split(',').join('');
}

const sendOtp = async (req, res, next) => {
    try {
        const { email } = req.body;
        const user = await User.findOne({ where: { email } });
        const otp = generateOTP();
        req.body.otp = otp;
        const expired = Date.now() + 60000;
        const authUpdate = await Authentication.update({ otp, expired }, {
            where: { user_id: user.id }
        });
        if (!authUpdate) throw new Error('OTP creation failed')
        const subPath = '../public/send-otp-mail.ejs';
        const subject = 'This is your OTP';
        const emailResponse = await mailer(req.body, subPath, subject);
        if (!emailResponse) throw new Error('send OTP failed');
        const recipients = emailResponse.accepted.join(',').split(', ');
        res.status(200).json({
            status: 'success',
            message: `Email successsfully sent to ${recipients}`
        })
    } catch (err) {
        next(err);
    }
}

module.exports = sendOtp;