const { User } = require('../models');
const crudHelper = require('../helper/crud.helper');

const createUser = (req, res, next) => {
    const { username, password, email } = req.body;
    const role_id = 1;
    const obj = { username, password, email, role_id };
    crudHelper.createHelper(req, res, next, User, 'User', obj);
}

module.exports = {
    createUser,
}

