const { User } = require('../models');
const crudHelper = require('../helper/crud.helper'); const bcrypt = require('bcrypt');
const saltRounds = process.env.SALT_ROUNDS;

const getAllUsers = (req, res, next) => {
    const attributes = ['id', 'username', 'password', 'email'];
    crudHelper.readHelper(req, res, next, User, 'Users', attributes);
}

const getUserById = (req, res, next) => {
    crudHelper.readByIdHelper(req, res, next, User, 'User');
}

const createUser = async (req, res, next) => {
    const { username, password, email } = req.body;
    const role_id = 2;
    const obj = { username, password, email, role_id };
    crudHelper.createHelper(req, res, next, User, 'User', obj);
}

const updateUser = async (req, res, next) => {
    let { username, password, email } = req.body;
    password = await bcrypt.hash(password, +saltRounds);
    const obj = { username, password, email }
    crudHelper.updateHelper(req, res, next, User, 'User', obj);
}

const deleteUser = (req, res, next) => {
    crudHelper.deleteHelper(req, res, next, 'User');
}

module.exports = {
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
}

