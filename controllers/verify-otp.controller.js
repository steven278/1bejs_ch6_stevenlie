require('dotenv').config();
const jwt = require('jsonwebtoken');
const { User, Authentication } = require('../models');
const bcrypt = require('bcrypt');
const saltRounds = process.env.SALT_ROUNDS;
const verifyOtp = async (req, res, next) => {
    try {
        const { email, otp } = req.body;
        const user = await User.findOne({ where: { email } });
        if (!user) throw new Error('user not found');
        const auth = await Authentication.findOne({ where: { user_id: user.id } });
        if (auth.otp === otp && auth.expired > Date.now()) {
            await Authentication.update({ isVerified: true }, { where: { user_id: user.id } });
            const hashedOtp = await bcrypt.hash(otp, +saltRounds);
            const payload = {
                id: user.id,
                email: user.email,
                otp: hashedOtp,
                expired: auth.expired,
                iat: Date.now()
            }
            const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 600000 });
            return res.status(200).json({ token })
        }
        throw new Error('OTP is incorrect or expired');
    } catch (err) {
        next(err);
    }

}

module.exports = verifyOtp;