const { User } = require('../models');
const bcrypt = require('bcrypt');
const saltRounds = process.env.SALT_ROUNDS;

const changePassword = async (req, res, next) => {
    try {
        const { newPassword, confirmNewPassword, email } = req.body;
        const newHashedPassword = await bcrypt.hash(newPassword, +saltRounds);
        const user = await User.update({ password: newHashedPassword }, {
            where: { email }
        });
        res.status(200).json({
            status: 'success',
            message: 'password change successful'
        })
    } catch (err) {
        next(err)
    }
}
module.exports = changePassword;