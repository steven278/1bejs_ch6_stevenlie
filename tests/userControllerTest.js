const userTest = () => {
    require('dotenv').config();
    const baseURL = process.env.BASE_URL || '/api/v1.0/';
    const app = require('../app');
    const request = require('supertest');

     //positive tests
    describe('getAllUsers function', () => { // get all user
        it('get all users data with success status, show data', async () => {
            const response = await request(app).get(`${baseURL}/users`);
            const { status, data } = response.body;
            expect(response.status).toBe(200);
            expect(status).toBe('success');
            expect(data.length).toBeGreaterThan(0);
        });
    });
    
    describe('getUserById function', () => { // get user by id
        it('get user data by Id with success status, show data', async () => {
            const response = await request(app).get(`${baseURL}/users/1`);
            const { status, data } = response.body;
            expect(response.status).toBe(200);
            expect(status).toBe('success');
            expect(data).toBeDefined();
        });
    });

    describe('deleteUser function', () => { // delete user by it's id
        it('delete user data by Id with status success, show message', async () => {
            const response = await request(app).delete(`${baseURL}/users/2`);
            const body = response.body;
            expect(response.status).toBe(200);
            expect(body).toMatchObject({
                status: "success",
                message: "User with id 2 has been removed"
            });
        });
    });

    describe('createUser function', () => { // create user with invalid email
        it('create user data with status 400, show error details', async () => {
            const body = { 
                username: "steveee",
                email: "steven@yahoo.com",
                password: "abcdefg"
            }
            const response = await request(app).post(`${baseURL}/users`)
                .send(body);
            const res = response.body;
            expect(response.status).toBe(201);
            expect(res).toMatchObject({
                "status": "success",
                "data": {
                    id: 6,
                    username: "steveee",
                    password: "abcdefg",
                    email: "steven@yahoo.com",
                    updatedAt: res.data.updatedAt,
                    createdAt: res.data.createdAt
                }
            })
            // expect(errors[0]).toMatchObject({
            //     "value": body.email,
            //     "msg": "invalid email input",
            //     "param": "email",
            //     "location": "body"
            // });
        });
    });
    
    //negative testss
    describe('createUser function', () => { // create user with invalid email
        it('create user data with status 400, show error details', async () => {
            const body = { 
                username: "steve",
                email: "steven.co.id",
                password: "asdasd"
            }
            const response = await request(app).post(`${baseURL}/users`)
                .send(body);
            const { errors } = response.body;
            expect(response.status).toBe(400);
            expect(errors[0]).toMatchObject({
                "value": body.email,
                "msg": "invalid email input",
                "param": "email",
                "location": "body"
            });
        });
    });
    
    describe('updateUser function', () => { // update user with invalid password (password less than 5 characters)
        it('update user data with status 400, show error details', async () => {
            const body = { 
                username: "steve",
                email: "steven@yahoo.co.id",
                password: "axs"
            }
            const response = await request(app).put(`${baseURL}/users/3`)
                .send(body);
            const { errors } = response.body;
            expect(response.status).toBe(400);
            expect(errors[0]).toMatchObject({
                "value": body.password,
                "msg": "must be at least 5 chars long",
                "param": "password",
                "location": "body"
            });
        });
    });
    
    describe('deleteUser function', () => { // delete user with invalid id
        it('delete user data by Id with status Not Found, show error message', async () => {
            const response = await request(app).delete(`${baseURL}/users/30`);
            const { status, errorName, message } = response.body;
            expect(response.status).toBe(404);
            expect(status).toBe('Not Found');
            expect(errorName).toBe('Error');
            expect(message).toBe('User with id 30 doesn\'t exist');
        });
    });
}

module.exports = userTest;