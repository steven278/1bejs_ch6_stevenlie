const profileTest = () => {
    require('dotenv').config();
    const baseURL = process.env.BASE_URL || '/api/v1.0/';
    const app = require('../app');
    const request = require('supertest');

    //positive tests
    describe('getAllProfiles function', () => { // get all profile
        it('get all profiles data with success status, show data', async () => {
            const response = await request(app).get(`${baseURL}/profiles`);
            const { status, data } = response.body;
            expect(response.status).toBe(200);
            expect(status).toBe('success');
            expect(data.length).toBeGreaterThan(0);
        });
    });
    
    describe('getProfileById function', () => { // get profile by id
        it('get profile data by Id with success status, show data', async () => {
            const response = await request(app).get(`${baseURL}/profiles/1`);
            const { status, data } = response.body;
            expect(response.status).toBe(200);
            expect(status).toBe('success');
            expect(data).toBeDefined();
        });
    });

    describe('updateProfile function', () => { // update a profile with user_id 1
        it('update profile data with status success, show data', async () => {
            const body = {
                user_country : "portugal",
                user_rank : "elite",
                gold_amount : 120000
            }
            const response = await request(app).put(`${baseURL}/profiles/1`)
                .send(body);
            const res = response.body;
            expect(response.status).toBe(200);
            expect(res).toMatchObject({
                status: "success",
                data: {
                    id: 1,
                    user_id: 1,
                    user_country: "portugal",
                    user_rank: "elite",
                    gold_amount: 120000,
                    createdAt: res.data.createdAt,
                    updatedAt: res.data.updatedAt
                }
            });
        });
    });
    
    //negative testss
    describe('createProfile function', () => { // create a profile for a user id who already has his own profile
        it('create profile data with status Not Found, show error details', async () => {
            const body = {
                user_id : 3,
                user_country : "Indonesia",
                user_rank : "master",
                gold_amount : 35000
            }
            const response = await request(app).post(`${baseURL}/profiles`)
                .send(body);
            const errors = response.body;
            expect(response.status).toBe(404);
            expect(errors).toMatchObject({
                "status": "Not Found",
                "errorName": "Error",
                "message": "profile with user_id 3 already exist"
            });
        });
    });
    
    describe('updateProfile function', () => { // update profile with invalid gold_amount data type
        it('update profile data with status 400, show error details', async () => {
            const body = {
                user_country : 'El Salvador',
                user_rank : 'warrior',
                gold_amount : 'banyak'
            }
            const response = await request(app).put(`${baseURL}/profiles/3`)
                .send(body);
            const { errors } = response.body;
            expect(response.status).toBe(400);
            expect(errors[0]).toMatchObject({
                "value": "banyak",
                "msg": "must be an integer with minimum value 0",
                "param": "gold_amount",
                "location": "body"
            });
        });
    });
    
    describe('deleteProfile function', () => { // delete profile with invalid id
        it('delete profile data by Id with status Not Found, show error message', async () => {
            const response = await request(app).delete(`${baseURL}/profiles/30`);
            const { status, errorName, message } = response.body;
            expect(response.status).toBe(404);
            expect(status).toBe('Not Found');
            expect(errorName).toBe('Error');
            expect(message).toBe('Profile with id 30 doesn\'t exist');
        });
    });
}

module.exports = profileTest;
