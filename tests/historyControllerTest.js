const historyTest = () => {
    require('dotenv').config();
    const baseURL = process.env.BASE_URL || '/api/v1.0/';
    const app = require('../app');
    const request = require('supertest');

    //positive tests
    describe('getAllHistories function', () => { // get all history
        it('get all histories data with success status, show data', async () => {
            const response = await request(app).get(`${baseURL}/histories`);
            const { status, data } = response.body;
            expect(response.status).toBe(200);
            expect(status).toBe('success');
            expect(data.length).toBeGreaterThan(0);
        });
    });
    
    describe('getHistoryById function', () => { // get history by id
        it('get history data by Id with success status, show data', async () => {
            const response = await request(app).get(`${baseURL}/histories/1`);
            const { status, data } = response.body;
            expect(response.status).toBe(200);
            expect(status).toBe('success');
            expect(data).toBeDefined();
        });
    });

    describe('createHistory function', () => { // create a history with user_id 1
        it('create history data with status success, show data', async () => {
            const body = {
                user_id : 1,
                match_duration_in_minutes: 35,
                match_score : 87
            }
            const response = await request(app).post(`${baseURL}/histories`)
                .send(body);
            const res = response.body;
            expect(response.status).toBe(201);
            expect(res).toMatchObject({
                status: "success",
                data: {
                    id: 11,
                    user_id: 1,
                    match_duration_in_minutes: 35,
                    match_score: 87,
                    updatedAt: res.data.updatedAt,
                    createdAt: res.data.createdAt
                }
            });
        });
    });
    
    // //negative testss
    describe('createHistory function', () => { // create a history with user_id that doesn't exist
        it('create history data with status Bad Request, show error details', async () => {
            const body = {
                user_id : 30,
                match_duration_in_minutes: 80,
                match_score : 95
            }
            const response = await request(app).post(`${baseURL}/histories`)
                .send(body);
            const errors = response.body;
            expect(response.status).toBe(400);
            expect(errors).toMatchObject({
                "status": "Bad Request",
                "errorName": "SequelizeForeignKeyConstraintError",
                "message": "insert or update on table \"Histories\" violates foreign key constraint \"Histories_user_id_fkey\""
            });
        });
    });
    
    describe('updateHistory function', () => { // update history with invalid match_duration data type
        it('update history data with status 400, show error details', async () => {
            const body = {
                "match_duration_in_minutes" : "seratus",
                "match_score" : 234
            }
            const response = await request(app).put(`${baseURL}/histories/3`)
                .send(body);
            const { errors } = response.body;
            expect(response.status).toBe(400);
            expect(errors[0]).toMatchObject({
                "value": "seratus",
                "msg": "must be an integer with minimum value 0",
                "param": "match_duration_in_minutes",
                "location": "body"
            });
        });
    });
    
    describe('deleteHistory function', () => { // delete history with invalid id
        it('delete history data by Id with status Not Found, show error message', async () => {
            const response = await request(app).delete(`${baseURL}/histories/30`);
            const { status, errorName, message } = response.body;
            expect(response.status).toBe(404);
            expect(status).toBe('Not Found');
            expect(errorName).toBe('Error');
            expect(message).toBe('History with id 30 doesn\'t exist');
        });
    });
}

module.exports = historyTest;