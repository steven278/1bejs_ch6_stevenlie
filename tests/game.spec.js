const { sequelize } = require('../models');

const getMasterData = (path) => {
    return require(path).map((eachData) => {
        eachData.createdAt = new Date();
        eachData.updatedAt = new Date();
        return eachData;
    });
}

const usersData = getMasterData('../masterdata/users.json');
const profilesData = getMasterData('../masterdata/profiles.json');
const historiesData =  getMasterData('../masterdata/histories.json');

beforeAll(async () => {
    try {
        await sequelize.queryInterface
            .bulkInsert('Users', usersData, {});
        await sequelize.queryInterface
            .bulkInsert('Profiles', profilesData, {});
        await sequelize.queryInterface
            .bulkInsert('Histories', historiesData, {});
    } catch (error) {
        console.log(error);
    }
})
afterAll(async () => {
    try {
        await sequelize.queryInterface
            .bulkDelete('Histories', null, { truncate: true, cascade:true, restartIdentity: true });
        await sequelize.queryInterface
            .bulkDelete('Profiles', null, { truncate: true, cascade:true, restartIdentity: true });
        await sequelize.queryInterface
            .bulkDelete('Users', null, { truncate: true, cascade:true, restartIdentity: true });
        await sequelize.close();
    } catch (error) {
        console.log(error);
    }
})

const userTest = require('./userControllerTest');
const profileTest  = require('./profileControllerTest');
const historyTest  = require('./historyControllerTest');

describe('run test sequentially', () => {
    userTest();
    profileTest();
    historyTest();
});

