const { check, validationResult } = require('express-validator');

const userValidation = () => {
    return [
        check('username').isLength({ min: 3 }).withMessage('must be at least 3 chars long'),
        check('email').isEmail().withMessage('invalid email input'),
        check('password').isLength({ min: 6 }).withMessage('must be at least 6 chars long')
    ]
}

const profileValidation = (type) => {
    const user_id = check('user_id').isInt({ min: 1 }).withMessage('must be an integer with minimum value 1');
    const user_country = check('user_country').isAlpha('en-US', { ignore: ' ' }).withMessage('string must contains only letters (a-zA-Z)');
    const user_rank = check('user_rank').isAlpha('en-US', { ignore: ' ' }).withMessage('string must contains only letters (a-zA-Z)');
    const gold_amount = check('gold_amount').isInt({ min: 0 }).withMessage('must be an integer with minimum value 0');
    if (type === 'create') {
        return [user_id, user_country, user_rank, gold_amount]
    }
    return [user_country, user_rank, gold_amount]
}

const historyValidation = (type) => {
    const user_id = check('user_id').isInt({ min: 1 }).withMessage('must be an integer with minimum value 1');
    const match_duration_in_minutes = check('match_duration_in_minutes').isInt({ min: 0 }).withMessage('must be an integer with minimum value 0');
    const match_score = check('match_score').isInt({ min: 0 }).withMessage('must be an integer with minimum value 0');
    if (type === 'create') {
        return [user_id, match_duration_in_minutes, match_score];
    }
    return [match_duration_in_minutes, match_score];
}

const resetPasswordValidation = (type) => {
    const email = check('email').isEmail().withMessage('invalid email input');
    if (type === 'verify-otp') {
        const otp = check('otp').isNumeric().isLength({ min: 6 }).withMessage('must be at least 6 chars long');
        return [email, otp];
    } else if (type === 'change-password') {
        const newPassword = check('newPassword').isLength({ min: 6 }).withMessage('must be at least 6 chars long');
        return [email, newPassword];
    }
    return [email];
}

const validate = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        return next();
    }
    return res.status(400).json({
        errors: errors.array()
    });
}

module.exports = {
    userValidation,
    profileValidation,
    historyValidation,
    resetPasswordValidation,
    validate
}