'use strict';

const profileData = require('../masterdata/profiles.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const profileDataTimestamped = profileData.map((e)=> {
      e['createdAt'] = new Date();
      e['updatedAt'] = new Date();
      return e
    })
    await queryInterface.bulkInsert('Profiles', profileDataTimestamped)
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Profiles', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
