'use strict';

const authentication = require('../masterdata/authentication.json');

module.exports = {
  async up(queryInterface, Sequelize) {
    const authTimestamped = authentication.map((e) => {
      e['createdAt'] = new Date();
      e['updatedAt'] = new Date();
      return e
    })
    await queryInterface.bulkInsert('Authentications', authTimestamped)
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Authentications', null, {});
  }
};
