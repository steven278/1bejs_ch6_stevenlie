'use strict';

require('dotenv').config();
const bcrypt = require('bcrypt');
const saltRounds = process.env.SALT_ROUNDS;

const userData = require('../masterdata/users.json');
const userDataTimestamped = userData.map(async (e) => {
  e.createdAt = new Date();
  e.updatedAt = new Date();
  e.password = await bcrypt.hash(e.password, +saltRounds);
  return e;
});

module.exports = {
  async up(queryInterface, Sequelize) {
    const usersHashed = await Promise.all(userDataTimestamped);
    await queryInterface.bulkInsert('Users', usersHashed, {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
