const router = require('express').Router();
const sendOtp = require('../controllers/send-otp.controller');
const { resetPasswordValidation, validate } = require('../validation/validator');

router.post('/', resetPasswordValidation('send-otp'), validate, sendOtp);

module.exports = router;