const router = require('express').Router();
const { createUser } = require('../controllers/register.controller');
const { userValidation, validate } = require('../validation/validator');
const ROLES = require('../utils/roles');
const authorization = require('../auth/passport');
const roleCheck = require('../auth/roleCheck');

router.post('/', authorization, roleCheck([ROLES.Admin]), userValidation(), validate, createUser);

module.exports = router;