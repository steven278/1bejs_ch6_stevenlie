const express = require('express');
const router = express.Router();
const { profileValidation, validate } = require('../validation/validator');
const { getAllProfiles, getProfileById, createProfile, updateProfile, deleteProfile } = require('../controllers/profiles.controller');
const ROLES = require('../utils/roles');
const authorization = require('../auth/passport');
const roleCheck = require('../auth/roleCheck');

router.get('/', authorization, roleCheck([ROLES.Admin]), getAllProfiles);
router.get('/:id', authorization, roleCheck([ROLES.Admin, ROLES.Member]), getProfileById);
router.post('/', authorization, roleCheck([ROLES.Admin, ROLES.Member]), profileValidation('create'), validate, createProfile);
router.put('/:id', authorization, roleCheck([ROLES.Admin, ROLES.Member]), profileValidation('update'), validate, updateProfile);
router.delete('/:id', authorization, roleCheck([ROLES.Admin, ROLES.Member]), deleteProfile);

module.exports = router;