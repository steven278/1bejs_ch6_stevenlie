const express = require('express');
const router = express.Router();
const upload = require('../helper/multer');
const uploadWithCloudinary = require('../helper/cloudinary');
const { historyValidation, validate } = require('../validation/validator');
const { getAllHistories, getHistoryById, createHistory, updateHistory, deleteHistory } = require('../controllers/histories.controller');
const { historySanitation } = require('../helper/history.sanitation');
const ROLES = require('../utils/roles');
const authorization = require('../auth/passport');
const roleCheck = require('../auth/roleCheck');

router.get('/', authorization, roleCheck([ROLES.Admin]), getAllHistories);
router.get('/:id', authorization, roleCheck([ROLES.Admin, ROLES.Member]), getHistoryById);
router.post('/', upload.single('replay_video'), authorization, roleCheck([ROLES.Admin, ROLES.Member]), uploadWithCloudinary, historySanitation, historyValidation('create'), createHistory);
router.put('/:id', upload.single('replay_video'), authorization, roleCheck([ROLES.Admin, ROLES.Member]), uploadWithCloudinary, historySanitation, historyValidation('update'), validate, updateHistory);
router.delete('/:id', authorization, roleCheck([ROLES.Admin, ROLES.Member]), deleteHistory);

module.exports = router;