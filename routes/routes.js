const express = require('express');
const router = express.Router();
const userRoutes = require('./users.routes');
const profileRoutes = require('./profiles.routes');
const historyRoutes = require('./histories.routes');
const loginRouter = require('./login.routes');
const register = require('./register.routes');
const sendOtp = require('./send-otp.routes');
const verifyOtp = require('./verify-otp.routes');
const changePassword = require('./change-password.routes');

router.get('/', (req, res) => {
    res.status(200).json({
        status: 'Success',
        message: 'hello world'
    })
})

router.use('/users', userRoutes);
router.use('/profiles', profileRoutes);
router.use('/histories', historyRoutes);
router.use('/login', loginRouter);
router.use('/register', register);
router.use('/send-otp', sendOtp);
router.use('/verify-otp', verifyOtp);
router.use('/change-password', changePassword);

router.use((err, req, res, next) => {
    if (err.name === 'SequelizeDatabaseError' || err.name === 'SequelizeUniqueConstraintError' || err.name === 'ReferenceError' || err.name === 'SequelizeForeignKeyConstraintError') {
        res.status(400).json({
            status: 'Bad Request',
            errorName: err.name,
            message: err.message
        });
    } else if (err.name === 'Error' || err.name === 'TypeError') {
        res.status(404).json({
            status: 'Not Found',
            errorName: err.name,
            message: err.message
        });
    }
    res.status(500).json({
        status: 'Internal Server Error',
        errorName: err.name,
        message: err.message
    });
})



module.exports = { router }