const router = require('express').Router();
const verifyOtp = require('../controllers/verify-otp.controller');
const { resetPasswordValidation, validate } = require('../validation/validator');

router.post('/', resetPasswordValidation('verify-otp'), validate, verifyOtp);

module.exports = router;