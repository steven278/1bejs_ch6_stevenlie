const express = require('express');
const router = express.Router();
const { userValidation, validate } = require('../validation/validator');
const { getAllUsers, getUserById, createUser, updateUser, deleteUser } = require('../controllers/users.controller');
const ROLES = require('../utils/roles');
const authorization = require('../auth/passport');
const roleCheck = require('../auth/roleCheck');


router.get('/', authorization, roleCheck([ROLES.Admin]), getAllUsers);
router.get('/:id', authorization, roleCheck([ROLES.Admin, ROLES.Member]), getUserById);
router.post('/', userValidation(), validate, createUser);
router.put('/:id', authorization, roleCheck([ROLES.Admin, ROLES.Member]), userValidation(), validate, updateUser);
router.delete('/:id', authorization, roleCheck([ROLES.Admin, ROLES.Member]), deleteUser);

module.exports = router;