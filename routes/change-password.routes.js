const router = require('express').Router();
const changePassword = require('../controllers/change-password.controller');
const { resetPasswordValidation, validate } = require('../validation/validator');
const authorization = require('../auth/passport');

router.put('/', resetPasswordValidation('change-password'), validate, authorization, changePassword);

module.exports = router;