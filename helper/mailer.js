"use strict";
require('dotenv').config();
const nodeMailer = require("nodemailer");
const ejs = require("ejs");
const path = require("path");

const mailer = (body, subPath, subject) => {
    let transporter = '';
    let mailOptions = '';
    ejs.renderFile(path.join(__dirname, subPath), body, {}, (err, html) => {
        if (err) console.log(err);
        mailOptions = {
            from: 'allpurpose369@gmail.com',
            to: body.email,
            subject,
            html
        }
        transporter = nodeMailer.createTransport({
            host: 'smtp-relay.sendinblue.com',
            port: 587,
            auth: {
                user: process.env.USER,
                pass: process.env.SECRET
            }
        });
    })
    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) reject(err);
            else resolve(info);
        });
    });
}


module.exports = mailer;