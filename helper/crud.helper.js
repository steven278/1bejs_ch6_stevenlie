const { History, Profile, User, Authentication } = require('../models');
const { validationResult, check } = require('express-validator');
const mailer = require('../helper/mailer');

module.exports = class crudHelper {
    static readHelper = async (req, res, next, model, modelName, attributes) => {
        try {
            let { page, row } = req.query;
            if (row == 0 || !page || !row) {
                page = 1;
                row = 5;
            }
            page -= page > 0 ? 1 : 0;
            page *= row;

            const options = {
                attributes,
                order: [['id', 'ASC']],
                limit: row,
                offset: page
            }
            const data = await model.findAll(options);
            if (data.length === 0) {
                throw new Error(`${modelName} data not found`);
            }
            res.status(200).json({
                status: 'success',
                data
            });
        } catch (err) {
            next(err);
        }
    }
    static readByIdHelper = async (req, res, next, model, modelName) => {
        try {
            const { id } = req.params;
            const options = { where: { id } }
            const data = await model.findOne(options);
            if (!data) {
                throw new Error(`cannot get ${modelName} by Id`);
            }
            res.status(200).json({
                status: 'success',
                data
            })
        } catch (err) {
            next(err);
        }
    }

    static createHelper = async (req, res, next, model, modelName, obj) => {
        try {
            const data = await model.create(obj);
            if (!data) {
                throw new Error(`cannot create ${modelName}`);
            }
            if (modelName === 'User') {
                const auth = await Authentication.create({ user_id: data.id })
                if (!auth) {
                    throw new Error('cannot create Authentication');
                }
                const subPath = '../public/welcome-mail.ejs';
                const subject = 'Welcome to our Game Platform';
                const emailResponse = await mailer(req.body, subPath, subject);
                const recipients = emailResponse.accepted.join(',').split(', ');
                res.status(201).json({
                    status: 'success',
                    message: `Email successsfully sent to ${recipients}`
                });
            } else {
                res.status(201).json({
                    status: 'success',
                    data
                })
            }
        } catch (err) {
            next(err);
        }
    }

    static updateHelper = async (req, res, next, model, modelName, obj) => {
        try {
            const data = await model.update(
                obj,
                {
                    where: { id: req.params.id },
                    plain: true,
                    returning: true,
                }
            );
            if (!data) {
                throw new Error(`cannot update ${modelName}`);
            }
            res.status(200).json({
                status: 'success',
                data: data[1]
            })
        } catch (err) {
            next(err);
        }
    }
    static deleteHelper = async (req, res, next, modelName) => {
        try {
            let data = '';
            const { id } = req.params;
            if (modelName === 'User') {
                data = await User.destroy({ where: { id } });
            } else if (modelName === 'Profile') {
                data = await Profile.destroy({ where: { id } });
            } else {
                data = await History.destroy({ where: { id } });
            }
            if (!data) {
                throw new Error(`${modelName} with id ${id} doesn't exist`);
            }
            res.status(200).json({
                status: 'success',
                message: `${modelName} with id ${id} has been removed`
            })
        } catch (err) {
            next(err);
        }
    }
}