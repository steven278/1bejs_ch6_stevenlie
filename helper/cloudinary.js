const cloudinary = require('cloudinary').v2;

cloudinary.config({
    cloud_name: 'awanmendung',
    api_key: '588366271337917',
    api_secret: 'qTo_f8mVg6hza3BkspOhEAC7CIU'
});

const uploadWithCloudinary = async (req, res, next) => {
    try {
        const foldering = `my-asset/${req.file.mimetype.split('/')[1]}`;
        const uploadResult = await cloudinary.uploader.upload(req.file.path, {
            resource_type: "video",
            folder: foldering
        });
        req.body.replay_video = uploadResult.secure_url;
        next();
    } catch (error) {
        console.log(error);
    }
};

module.exports = uploadWithCloudinary;
