const historySanitation = (req, res, next) => {
    req.body.user_id = parseInt(req.body.user_id);
    req.body.match_duration_in_minutes = parseInt(req.body.match_duration_in_minutes);
    req.body.match_score = parseInt(req.body.match_score);
    next();
}

module.exports = {
    historySanitation
}