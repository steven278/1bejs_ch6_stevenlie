'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
const saltRounds = process.env.SALT_ROUNDS;
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      // define association here
      User.hasOne(models.Profile, {
        foreignKey: 'user_id',
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      });
      User.hasOne(models.Authentication, {
        foreignKey: 'user_id',
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      });
      User.hasMany(models.History, {
        foreignKey: 'user_id',
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      });
    }
  }
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    role_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User',
    hooks: {
      beforeCreate: async (user, options) => {
        user.password = await bcrypt.hash(user.password, +saltRounds);
        return user;
      },
    }
  });
  return User;
};